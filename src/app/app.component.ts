import {Component} from '@angular/core';
import {Post} from './model/Post';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog';


  posts: Post[] = [
    {
      title: "Mon premier post",
      content: "Lorem upsum",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Mon deuxième post",
      content: "Lorem upsum",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Encore un post",
      content: "Lorem upsum",
      loveIts: 0,
      created_at: new Date()
    }
  ]
}
