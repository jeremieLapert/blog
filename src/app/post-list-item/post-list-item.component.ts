import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() post: object;

  loveIts =0;

  constructor() { }

  ngOnInit() {
  }

  onLike() {
    this.loveIts += 1;
  }

  onDislike() {
    this.loveIts -= 1;
  }

  getStatus() {
    if (this.loveIts > 0) {
      return 'success';
    } else if (this.loveIts < 0) {
      return 'danger'
    }
  }
}
